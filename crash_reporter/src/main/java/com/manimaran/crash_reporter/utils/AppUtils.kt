package com.manimaran.crash_reporter.utils

import android.content.Context
import android.os.Build
import android.text.format.DateFormat
import java.util.*


object AppUtils {

    @JvmStatic
    fun getDeviceDetails(context: Context): String {
        return """
        DATE & TIME : ${DateFormat.format("dd-MM-yyyy hh:mm:ss a", Date())}
        APP.VERSION : ${getAppVersionName(context)}_${getAppVersion(context)}
        TIMEZONE : ${timeZone()}
        VERSION.RELEASE : ${Build.VERSION.RELEASE} & SDK : ${Build.VERSION.SDK_INT} 
        BRAND & MODEL : ${Build.BRAND} & ${Build.MODEL}
        DEVICE : ${Build.DEVICE}
        CPU_ABI : ${Build.CPU_ABI}
        TIMESTAMP : ${Build.TIME}""".trimIndent()
    }

    private fun timeZone(): String {
        val tz = TimeZone.getDefault()
        return tz.id
    }

    private fun getAppVersion(context: Context): Long {
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        var versionCode: Long = 0
        try {
            versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                pInfo.longVersionCode
            } else {
                pInfo.versionCode.toLong()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return versionCode
    }

    private fun getAppVersionName(context: Context): String {
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        return pInfo.versionName
    }

    fun getApplicationName(context: Context): String {
        val applicationInfo = context.applicationInfo
        val stringId = applicationInfo.labelRes
        return if (stringId == 0) applicationInfo.nonLocalizedLabel.toString() else context.getString(
            stringId
        )
    }
}